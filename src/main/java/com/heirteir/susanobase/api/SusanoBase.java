package com.heirteir.susanobase.api;

import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;

@Getter
public final class SusanoBase extends JavaPlugin {

  public SusanoBase() {
    super();
  }

  @Override
  public void onEnable() {
    SusanoAPIAccess.load(this);
  }

  @Override
  public void onDisable() {
    SusanoAPIAccess.unload();
  }
}
