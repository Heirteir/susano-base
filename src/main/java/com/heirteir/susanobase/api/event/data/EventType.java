package com.heirteir.susanobase.api.event.data;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum EventType {
  PACKET(GenericType.PACKET);
  private final GenericType genericType;

  public enum GenericType {
    PACKET,
    EVENT
  }
}
