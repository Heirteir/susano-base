package com.heirteir.susanobase.api.event;

import com.heirteir.susanobase.api.SusanoAPI;
import com.heirteir.susanobase.api.event.data.EventInfo;
import com.heirteir.susanobase.api.packets.PacketType;
import com.heirteir.susanobase.api.packets.wrappers.WrappedPacket;
import com.heirteir.susanobase.api.player.SusanoPlayer;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
public abstract class AbstractPacketEvent<T extends WrappedPacket> {

  private final EventInfo info;
  private final PacketType packetType;

  public AbstractPacketEvent(EventInfo info, PacketType packetType) {
    this.info = info;
    this.packetType = packetType;
  }

  public abstract boolean update(SusanoAPI api, SusanoPlayer player, @NotNull T packet);
}
