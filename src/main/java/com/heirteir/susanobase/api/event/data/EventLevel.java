package com.heirteir.susanobase.api.event.data;

public enum EventLevel {
  PRE_PROCESS,
  PROCESS_1,
  PROCESS_2,
  PROCESS_3,
  POST_PROCESS
}
