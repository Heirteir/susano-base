package com.heirteir.susanobase.api.event.data;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class EventInfo {

  private final EventLevel level;
  private final EventType event;
}
