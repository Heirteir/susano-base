package com.heirteir.susanobase.api.event;

import com.heirteir.susanobase.api.SusanoAPI;
import com.heirteir.susanobase.api.event.data.EventInfo;
import com.heirteir.susanobase.api.player.SusanoPlayer;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public abstract class AbstractEvent {

  private final EventInfo info;

  public abstract boolean update(SusanoAPI api, SusanoPlayer player);
}
