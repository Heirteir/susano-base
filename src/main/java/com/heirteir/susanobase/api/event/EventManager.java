package com.heirteir.susanobase.api.event;

import com.heirteir.susanobase.api.SusanoAPI;
import com.heirteir.susanobase.api.event.data.EventType;
import com.heirteir.susanobase.api.packets.PacketType;
import com.heirteir.susanobase.api.packets.wrappers.WrappedPacket;
import com.heirteir.susanobase.api.player.SusanoPlayer;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@SuppressWarnings("unchecked") // Type Erasure [unavoidable]
public final class EventManager {

  private final List<AbstractPacketEvent> packetEvents;
  private final List<AbstractEvent> genericEvents;

  public EventManager() {
    this.packetEvents = new ArrayList<AbstractPacketEvent>() {
      @Override
      public boolean add(AbstractPacketEvent event) {
        super.add(event);
        super.sort(Comparator.comparingInt(e -> e.getInfo().getLevel().ordinal()));
        return true;
      }
    };
    this.genericEvents = new ArrayList<AbstractEvent>() {
      @Override
      public boolean add(AbstractEvent event) {
        super.add(event);
        super.sort(Comparator.comparingInt(e -> e.getInfo().getLevel().ordinal()));
        return true;
      }
    };
  }

  /**
   * Add a PacketEvent to the Event Manager.
   *
   * @param event the PacketEvent
   */
  public void addPacketEvent(AbstractPacketEvent event) {
    this.packetEvents.add(event);
  }

  /**
   * Remove a PacketEvent from the Event Manager.
   *
   * @param event the PacketEvent
   */
  public void removePacketEvent(AbstractPacketEvent event) {
    this.packetEvents.remove(event);
  }

  /**
   * Add an Event to the Event Manager.
   *
   * @param event the Event
   */
  public void addEvent(AbstractEvent event) {
    this.genericEvents.add(event);
  }

  /**
   * Remove an Event from the Event Manager.
   *
   * @param event the Event
   */
  public void removeEvent(AbstractEvent event) {
    this.genericEvents.remove(event);
  }

  public void runPacketUpdaters(SusanoAPI api, SusanoPlayer player, PacketType packetType, WrappedPacket packet) {
    assert packet != null;
    this.packetEvents.stream()
        .filter(event -> event.getPacketType().equals(packetType))
        .filter(event -> !event.update(api, player, packet))
        .findFirst(); // if update returns false that means stop the loop so we end it.
  }

  public void runEventUpdaters(SusanoAPI api, SusanoPlayer player, EventType eventType) {
    this.genericEvents.stream()
        .filter(event -> event.getInfo().getEvent().equals(eventType))
        .filter(event -> !event.update(api, player))
        .findFirst(); // if update return false that means stop the loop so we end it.
  }
}
