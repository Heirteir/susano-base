package com.heirteir.susanobase.api.util.reflections.types;

import com.heirteir.susanobase.api.SusanoAPI;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import javax.annotation.Nullable;
import lombok.Getter;

@SuppressWarnings("unchecked") // Type Erasure [unavoidable]
@Getter
public class WrappedField {

  private final SusanoAPI api;
  private final WrappedClass parent;
  private final Field raw;

  protected WrappedField(SusanoAPI api, WrappedClass parent, Field raw) {
    this.api = api;
    this.parent = parent;
    this.raw = raw;
    this.raw.setAccessible(true);
  }

  /**
   * Get value of field from instance object.
   *
   * @param instance Object instance to get value from
   * @param <T>      type to convert to
   * @return value of field
   */
  public <T> T get(@Nullable Object instance) {
    T object = null;
    try {
      object = (T) this.raw.get(instance);
    } catch (IllegalAccessException e) {
      this.api.getLogger().severe(e.getMessage(), e);
    }
    return object;
  }

  /**
   * Set the value of field inside of instance object.
   *
   * @param instance Object instance to change the field value in
   * @param value    new Value for specified Field
   * @return Current Instance of WrappedField
   */
  public WrappedField set(@Nullable Object instance, Object value) {
    try {
      Field modifiersField = Field.class.getDeclaredField("modifiers");
      modifiersField.setAccessible(true);
      modifiersField.setInt(this.raw, this.raw.getModifiers() & ~Modifier.FINAL);
      this.raw.set(instance, value);
    } catch (IllegalAccessException | NoSuchFieldException e) {
      this.api.getLogger().severe(e.getMessage(), e);
    }
    return this;
  }
}
