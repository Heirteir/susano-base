package com.heirteir.susanobase.api.util.reflections.types;

import com.heirteir.susanobase.api.SusanoAPI;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import javax.annotation.Nullable;
import lombok.Getter;

@SuppressWarnings("unchecked") // Type Erasure [unavoidable]
@Getter
public class WrappedMethod {

  private final SusanoAPI api;
  private final WrappedClass parent;
  private final Method raw;

  protected WrappedMethod(SusanoAPI api, WrappedClass parent, Method raw) {
    this.api = api;
    this.parent = parent;
    this.raw = raw;
    this.raw.setAccessible(true);
  }

  /**
   * Method used to invoke method from instance object.
   *
   * @param instance object instance to invoke method in
   * @param args     arguments for invoked method
   * @param <T>      expected return type
   * @return value returned from invoked method
   */
  public <T> T invoke(@Nullable Object instance, Object... args) {
    T object = null;
    try {
      object = (T) this.raw.invoke(instance, args);
    } catch (IllegalAccessException | InvocationTargetException e) {
      this.api.getLogger().severe(e.getMessage(), e);
    }
    return object;
  }
}
