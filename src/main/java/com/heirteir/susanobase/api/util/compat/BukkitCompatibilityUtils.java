package com.heirteir.susanobase.api.util.compat;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public final class BukkitCompatibilityUtils {

  private BukkitCompatibilityUtils() {}

  public static Player[] getAllPlayers() {
    return Bukkit.getWorlds().stream().flatMap(world -> world.getPlayers().stream()).toArray(Player[]::new);
  }
}
