package com.heirteir.susanobase.api.util.math;

import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.World;

@Getter
public final class Vector3D {

  private double x;
  private double y;
  private double z;

  public Vector3D(double x, double y, double z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }

  public Vector3D(Location location) {
    this(location.getX(), location.getY(), location.getZ());
  }

  public Vector3D(Vector2D vector) {
    this(vector.getX(), 0, vector.getY());
  }

  public Vector3D add(double x, double y, double z) {
    this.x += x;
    this.y += y;
    this.z += z;
    return this;
  }

  public Vector3D add(Vector3D vector) {
    return this.add(vector.x, vector.y, vector.z);
  }

  public Vector3D add(double amount) {
    return this.add(amount, amount, amount);
  }

  public Vector3D subtract(double x, double y, double z) {
    this.x -= x;
    this.y -= y;
    this.z -= z;
    return this;
  }

  public Vector3D subtract(Vector3D vector) {
    return this.subtract(vector.x, vector.y, vector.z);
  }

  public Vector3D subtract(double amount) {
    return this.subtract(amount, amount, amount);
  }

  public Vector3D multiply(double x, double y, double z) {
    this.x *= x;
    this.y *= y;
    this.z *= z;
    return this;
  }

  public Vector3D multiply(Vector3D vector) {
    return this.multiply(vector.x, vector.y, vector.z);
  }

  public Vector3D multiply(double amount) {
    return this.multiply(amount, amount, amount);
  }

  public Vector3D divide(double x, double y, double z) {
    this.x /= x;
    this.y /= y;
    this.z /= z;
    return this;
  }

  public Vector3D divide(Vector3D vector) {
    return this.divide(vector.x, vector.y, vector.z);
  }

  public Vector3D divide(double amount) {
    return this.divide(amount, amount, amount);
  }

  public double dot(Vector3D vector) {
    return this.x * vector.x + this.y * vector.y + this.z * vector.z;
  }

  public double angle(Vector3D other) {
    return Math.acos(this.dot(other) / (this.length() * other.length()));
  }

  public Vector3D set(double x, double y, double z) {
    this.x = x;
    this.y = y;
    this.z = z;
    return this;
  }

  public Vector3D setX(double x) {
    this.x = x;
    return this;
  }

  public Vector3D setY(double y) {
    this.y = y;
    return this;
  }

  public Vector3D setZ(double z) {
    this.z = z;
    return this;
  }

  public Vector3D addX(double x) {
    this.x += x;
    return this;
  }

  public Vector3D addY(double y) {
    this.y += y;
    return this;
  }

  public Vector3D addZ(double z) {
    this.z += z;
    return this;
  }

  public Vector2D toVector2D() {
    return new Vector2D(this.x, this.z);
  }

  public Vector3D copy() {
    return new Vector3D(this.x, this.y, this.z);
  }

  public Location toLocation(World world) {
    return new Location(world, this.x, this.y, this.z);
  }

  public double length() {
    return Math.sqrt(this.lengthSquared());
  }

  public double lengthSquared() {
    return this.x * this.x + this.y * this.y + this.z * this.z;
  }

  @Override
  public String toString() {
    return "[x=" + this.x + ",y=" + this.y + ",z=" + this.z + "]";
  }
}
