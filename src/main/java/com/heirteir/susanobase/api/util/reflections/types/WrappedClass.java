package com.heirteir.susanobase.api.util.reflections.types;

import com.google.common.collect.Lists;
import com.heirteir.susanobase.api.SusanoAPI;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@SuppressWarnings("unchecked") // Type Erasure [unavoidable]
@RequiredArgsConstructor
@Getter
public final class WrappedClass {

  private final SusanoAPI api;
  private final Class<?> raw;

  public WrappedConstructor getConstructorAtIndex(int index) {
    return new WrappedConstructor(this.api, this, this.raw.getDeclaredConstructors()[index]);
  }

  /**
   * Get constructor inside of class.
   *
   * @param types the parameters types for the specified constructor
   * @return WrappedConstructor instance of the constructor
   */
  public WrappedConstructor getConstructor(Class<?>... types) {
    WrappedConstructor wrappedConstructor = null;
    try {
      wrappedConstructor = new WrappedConstructor(this.api, this, this.raw.getDeclaredConstructor(types));
    } catch (NoSuchMethodException e) {
      this.api.getLogger().severe(e.getMessage(), e);
    }
    return wrappedConstructor;
  }

  private List<Field> getAllFields() {
    Class<?> type = this.raw;
    List<Field> fields = Lists.newArrayList(type.getDeclaredFields());
    while (true) {
      type = type.getSuperclass();
      if (type == null) {
        break;
      }
      fields.addAll(Arrays.asList(type.getDeclaredFields()));
    }
    return fields;
  }

  public boolean hasFieldByName(String name) {
    return this.getAllFields().stream().anyMatch(field -> field.getName().equals(name));
  }

  /**
   * Gets a specified field by type.
   *
   * @param type  type of field
   * @param index if multiple fields exist get value at specified index
   * @return WrappedField instance of the field
   */
  public WrappedField getFieldByType(Class<?> type, int index) {
    List<Field> validFields = this.getAllFields().stream().filter(field -> field.getType().equals(type)).collect(Collectors.toList());
    return new WrappedField(this.api, this, validFields.get(index));
  }

  /**
   * Get a field by name instead of type.
   *
   * @param name Name of the field
   * @return WrappedField instance of the field
   */
  public WrappedField getFieldByName(String name) {
    return new WrappedField(this.api, this, this.getAllFields().stream().filter(field -> field.getName().equals(name)).findFirst().orElse(null));
  }

  /**
   * Get a method using name and parameters.
   *
   * @param name       Method name
   * @param parameters Method parameters
   * @return WrappedMethod instance of the method
   */
  public WrappedMethod getMethod(String name, Class<?>... parameters) {
    return new WrappedMethod(this.api,
        this,
        Stream.concat(Arrays.stream(this.raw.getDeclaredMethods()), Arrays.stream(this.raw.getMethods()))
            .filter(method -> method.getName().equals(name) && parameters.length == method.getParameterCount() && Arrays.equals(parameters, method.getParameterTypes()))
            .findFirst()
            .orElse(null));
  }

  public Enum<?> getEnum(String name) {
    return Enum.valueOf((Class) this.raw, name);
  }
}
