package com.heirteir.susanobase.api.util.reflections.helpers;

import com.heirteir.susanobase.api.util.reflections.ReflectionsLibrary;
import com.heirteir.susanobase.api.util.reflections.helpers.child.entity.EntityHelper;
import com.heirteir.susanobase.api.util.reflections.helpers.child.entity.PlayerHelper;
import lombok.Getter;

@Getter
public class HelperMethods {

  private final PlayerHelper playerHelper;
  private final EntityHelper entityHelper;

  public HelperMethods(ReflectionsLibrary reflections) {
    this.playerHelper = new PlayerHelper(reflections);
    this.entityHelper = new EntityHelper(reflections);
  }
}
