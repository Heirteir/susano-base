package com.heirteir.susanobase.api.util.reflections.helpers.child.entity;

import com.heirteir.susanobase.api.util.reflections.ReflectionsLibrary;
import com.heirteir.susanobase.api.util.reflections.Version;
import com.heirteir.susanobase.api.util.reflections.types.WrappedField;
import com.heirteir.susanobase.api.util.reflections.types.WrappedMethod;
import org.bukkit.entity.Player;

public final class PlayerHelper {

  private static final int ELYTRA_FLYING_FLAG = 7;
  private final ReflectionsLibrary reflections;
  /* Player - Get */
  private final WrappedMethod getHandle;
  private final WrappedField playerConnection;
  private final WrappedMethod sendPacket;
  /* Player - Elytra */
  private WrappedMethod getFlag;

  public PlayerHelper(ReflectionsLibrary reflections) {
    this.reflections = reflections;

    /* Player - Get */
    this.getHandle = reflections.getCraftBukkitClass("entity.CraftEntity").getMethod("getHandle");
    this.playerConnection = reflections.getNetMinecraftServerClass("EntityPlayer").getFieldByName("playerConnection");
    this.sendPacket = reflections.getNetMinecraftServerClass("PlayerConnection").getMethod("sendPacket", reflections.getNetMinecraftServerClass("Packet").getRaw());

    /* Player - Elytra */
    if (this.reflections.getVersion().greaterThanOrEqual(Version.NINE_R1)) {
      this.getFlag = reflections.getNetMinecraftServerClass("Entity").getMethod("getFlag", int.class);
    }
  }

  /**
   * Returns whether or not the specified player is flying using Elytra.
   *
   * @param player The Bukkit Player
   * @return true if player is flying with elytra false if not;
   */
  public boolean isElytraFlying(Player player) {
    return this.reflections.getVersion().greaterThanOrEqual(Version.NINE_R1) && (boolean) this.getFlag.invoke(this.getEntityPlayer(player), PlayerHelper.ELYTRA_FLYING_FLAG);
  }

  /**
   * Convert a player object to an EntityPlayer object.
   *
   * @param player The Bukkit Player
   * @return An EntityPlayer object.
   */
  public Object getEntityPlayer(Player player) {
    return this.getHandle.invoke(player);
  }

  /**
   * Retrieves the PlayerConnection from the player.
   *
   * @param player The Bukkit Player
   * @return A PlayerConnection object.
   */
  public Object getPlayerConnection(Player player) {
    return this.playerConnection.get(this.getEntityPlayer(player));
  }

  /**
   * Sends a packet to the specified player object.
   *
   * @param player The Bukkit Player
   * @param packet The Packet object
   */
  public void sendPacket(Player player, Object packet) {
    this.sendPacket.invoke(this.playerConnection.get(this.getEntityPlayer(player)), packet);
  }
}
