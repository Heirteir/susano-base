package com.heirteir.susanobase.api.util.reflections.types;

import com.heirteir.susanobase.api.SusanoAPI;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import lombok.Getter;

@SuppressWarnings("unchecked") // Type Erasure [unavoidable]
@Getter
public class WrappedConstructor {

  private final SusanoAPI api;
  private final WrappedClass parent;
  private final Constructor<?> raw;

  protected WrappedConstructor(SusanoAPI api, WrappedClass parent, Constructor<?> raw) {
    this.api = api;
    this.parent = parent;
    this.raw = raw;
    this.raw.setAccessible(true);
  }

  /**
   * Create new instance of an object.
   *
   * @param arguments The arguments to pass to the new instance
   * @param <T>       Expected type of constructed value
   * @return New Instance of object
   */
  public <T> T newInstance(Object... arguments) {
    T object = null;
    try {
      object = (T) this.raw.newInstance(arguments);
    } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
      this.api.getLogger().severe(e.getMessage(), e);
    }
    return object;
  }
}
