package com.heirteir.susanobase.api.util.reflections;

import com.heirteir.susanobase.api.SusanoAPI;
import com.heirteir.susanobase.api.util.reflections.helpers.HelperMethods;
import com.heirteir.susanobase.api.util.reflections.types.WrappedClass;
import lombok.Getter;
import org.bukkit.Bukkit;

public final class ReflectionsLibrary {

  private final SusanoAPI api;
  @Getter
  private final Version version;
  private final String craftBukkitString;
  private final String netMinecraftServerString;
  @Getter
  private HelperMethods helper;

  public ReflectionsLibrary(SusanoAPI api) {
    this.api = api;
    this.version = Version.fromPackage(Bukkit.getServer().getClass().getPackage().getName().split("v")[1]);
    this.craftBukkitString = "org.bukkit.craftbukkit.v" + this.version.getPackageName() + ".";
    this.netMinecraftServerString = "net.minecraft.server.v" + this.version.getPackageName() + ".";
  }

  public void load() {
    this.helper = new HelperMethods(this);
  }

  public boolean classExists(String name) {
    boolean exists = false;
    try {
      Class.forName(name);
      exists = true;
    } catch (ClassNotFoundException ignored) {
    }
    return exists;
  }

  public WrappedClass getCraftBukkitClass(String name) {
    return this.getClass(this.craftBukkitString + name);
  }

  public WrappedClass getNetMinecraftServerClass(String name) {
    return this.getClass(this.netMinecraftServerString + name);
  }

  public WrappedClass getClass(String name) {
    assert this.classExists(name);
    WrappedClass wrappedClass = null;
    try {
      wrappedClass = new WrappedClass(this.api, Class.forName(name));
    } catch (ClassNotFoundException ignored) {
    }
    return wrappedClass;
  }
}
