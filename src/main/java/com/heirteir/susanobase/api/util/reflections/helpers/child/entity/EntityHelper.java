package com.heirteir.susanobase.api.util.reflections.helpers.child.entity;

import com.heirteir.susanobase.api.util.reflections.ReflectionsLibrary;
import com.heirteir.susanobase.api.util.reflections.Version;
import com.heirteir.susanobase.api.util.reflections.types.WrappedMethod;

public class EntityHelper {

  /* 1.7 - 1.12 */
  private Object RegistryMaterials;
  private WrappedMethod getKey;
  private WrappedMethod getEntityId;
  private WrappedMethod registerEntity;

  /* 1.13 - 1.16 */

  public EntityHelper(ReflectionsLibrary reflections) {
    if (reflections.getVersion().lessThanOrEqual(Version.TWELVE_R1)) {
      this.RegistryMaterials = reflections.getNetMinecraftServerClass("EntityTypes").getFieldByName("b").get(null);
      this.getKey = reflections.getNetMinecraftServerClass("RegistryMaterials").getMethod("b", Object.class);
      this.getEntityId = reflections.getNetMinecraftServerClass("RegistryMaterials").getMethod("a", Object.class);
      this.registerEntity = reflections.getNetMinecraftServerClass("RegistryMaterials").getMethod("a", int.class, Object.class, Object.class);
    } else {

    }
  }

  public void overwriteEntity(Class<?> nmsEntityClass, Class<?> customEntityClass) {
    int entityId = this.getEntityId.invoke(this.RegistryMaterials, nmsEntityClass);
    Object key = this.getKey.invoke(this.RegistryMaterials, nmsEntityClass);

    this.registerEntity.invoke(this.RegistryMaterials, entityId, key, customEntityClass);
  }
}
