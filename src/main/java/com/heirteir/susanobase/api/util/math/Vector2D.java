package com.heirteir.susanobase.api.util.math;

import lombok.Getter;
import org.bukkit.Location;

@Getter
public final class Vector2D {

  private double x;
  private double y;

  public Vector2D(double x, double y) {
    this.x = x;
    this.y = y;
  }

  public Vector2D(Location location) {
    this(location.getYaw(), location.getPitch());
  }

  public Vector2D add(double x, double y) {
    this.x += x;
    this.y += y;
    return this;
  }

  public Vector2D add(Vector2D vector) {
    return this.add(vector.x, vector.y);
  }

  public Vector2D add(double amount) {
    return this.add(amount, amount);
  }

  public Vector2D subtract(double x, double y) {
    this.x -= x;
    this.y -= y;
    return this;
  }

  public Vector2D subtract(Vector2D vector) {
    return this.subtract(vector.x, vector.y);
  }

  public Vector2D subtract(double amount) {
    return this.subtract(amount, amount);
  }

  public Vector2D multiply(double x, double y) {
    this.x *= x;
    this.y *= y;
    return this;
  }

  public Vector2D multiply(Vector2D vector) {
    return this.multiply(vector.x, vector.y);
  }

  public Vector2D multiply(double amount) {
    return this.multiply(amount, amount);
  }

  public Vector2D divide(double x, double y) {
    this.x /= x;
    this.y /= y;
    return this;
  }

  public Vector2D divide(Vector2D vector) {
    return this.divide(vector.x, vector.y);
  }

  public Vector2D divide(double amount) {
    return this.divide(amount, amount);
  }

  public Vector2D copy() {
    return new Vector2D(this.x, this.y);
  }

  public Vector3D toVector3D() {
    double xz = Math.cos(Math.toRadians(this.getY()));
    return new Vector3D(-xz * Math.sin(Math.toRadians(this.getX())), -Math.sin(Math.toRadians(this.getY())), xz * Math.cos(Math.toRadians(this.getX())));
  }

  public double dot(Vector2D vector) {
    return this.x * vector.x + this.y * vector.y;
  }

  public double angle(Vector2D other) {
    return Math.acos(this.dot(other) / (this.length() * other.length()));
  }

  public double length() {
    return Math.sqrt(this.lengthSquared());
  }

  public double lengthSquared() {
    return this.x * this.x + this.y * this.y;
  }

  public Vector2D setX(double x) {
    this.x = x;
    return this;
  }

  public Vector2D setY(double y) {
    this.y = y;
    return this;
  }

  @Override
  public String toString() {
    return "[x=" + this.x + ",y=" + this.y + "]";
  }
}
