package com.heirteir.susanobase.api.util.reflections;

import java.util.Arrays;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum Version {
  MIN(""),
  SEVEN_R4("1_7_R4"),
  EIGHT_R1("1_8_R1"),
  EIGHT_R2("1_8_R2"),
  EIGHT_R3("1_8_R3"),
  NINE_R1("1_9_R1"),
  NINE_R2("1_9_R2"),
  TEN_R1("1_10_R1"),
  ELEVEN_R1("1_11_R1"),
  TWELVE_R1("1_12_R1"),
  THIRTEEN_R1("1_13_R1"),
  THIRTEEN_R2("1_13_R2"),
  FOURTEEN_R1("1_14_R1"),
  FIFTEEN_R1("1_15_R1"),
  SIXTEEN_R1("1_16_R1"),
  SIXTEEN_R2("1_16_R2"),
  MAX(""),
  INVALID("");
  private final String packageName;

  protected static Version fromPackage(String packageName) {
    return Arrays.stream(Version.values()).filter(version -> version.getPackageName().equals(packageName)).findFirst().orElse(Version.INVALID);
  }

  public boolean greaterThanOrEqual(Version other) {
    return this.ordinal() >= other.ordinal();
  }

  public boolean lessThanOrEqual(Version other) {
    return this.ordinal() <= other.ordinal();
  }

  public boolean isValid() {
    return !this.equals(Version.INVALID);
  }
}
