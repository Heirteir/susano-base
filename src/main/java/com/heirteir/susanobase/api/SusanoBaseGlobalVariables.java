package com.heirteir.susanobase.api;

import java.io.File;
import lombok.Getter;
import lombok.Setter;

@Getter
public final class SusanoBaseGlobalVariables {

  private final SusanoAPI api;
  @Setter
  private String pluginPrefix;
  /* Directories */
  private File dataFolder;
  private File loggingFolder;

  public SusanoBaseGlobalVariables(SusanoAPI api) {
    this.api = api;
    this.pluginPrefix = "Susano";
  }

  public void load() {
    this.setDataFolder("Susano Anti-Cheat");
  }

  public void setDataFolder(String folderName) {
    this.dataFolder = new File(this.api.getBase().getDataFolder(), folderName);
    this.loggingFolder = new File(this.dataFolder, "logs");
    this.createDirectory(this.dataFolder);
    this.createDirectory(this.loggingFolder);
  }

  private void createDirectory(File file) {
    if (file.mkdirs()) {
      this.api.getLogger().info(String.format("Created directory at: %s", file.getAbsolutePath()));
    } else {
      if (!file.exists()) {
        this.api.getLogger().info(String.format("Failed to create directory at: %s", file.getAbsolutePath()));
      }
    }
  }
}
