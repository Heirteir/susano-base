package com.heirteir.susanobase.api;

import com.heirteir.susanobase.api.event.EventManager;
import com.heirteir.susanobase.api.logging.SusanoLogger;
import com.heirteir.susanobase.api.packets.handling.PacketManager;
import com.heirteir.susanobase.api.player.SusanoPlayerList;
import com.heirteir.susanobase.api.util.reflections.ReflectionsLibrary;
import lombok.Getter;

@Getter
public final class SusanoAPI {

  private final SusanoBase base;
  private final ReflectionsLibrary reflections;
  private SusanoBaseGlobalVariables globalVariables;
  private SusanoPlayerList playerList;
  private PacketManager packetManager;
  private EventManager eventManager;
  private SusanoLogger logger;

  public SusanoAPI(SusanoBase base) {
    this.base = base;
    this.reflections = new ReflectionsLibrary(this);
    if (this.reflections.getVersion().isValid()) {
      // create classes
      this.globalVariables = new SusanoBaseGlobalVariables(this);
      this.playerList = new SusanoPlayerList(this);
      this.packetManager = new PacketManager(this);
      this.eventManager = new EventManager();
      this.logger = new SusanoLogger(this.globalVariables);
      // load classes
      this.reflections.load();
      this.globalVariables.load();
      this.packetManager.load();
      this.playerList.load();
    }
  }

  public void unload() {
    if (this.reflections.getVersion().isValid()) {
      this.playerList.unload();
      this.packetManager.unload();
      this.logger.unload();
    }
  }
}
