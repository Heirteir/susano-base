package com.heirteir.susanobase.api.packets.wrappers.builder.conversion;

import com.google.common.collect.Lists;
import com.heirteir.susanobase.api.util.reflections.types.WrappedField;
import java.util.List;
import java.util.function.Function;

@SuppressWarnings("unchecked")
public final class ConvertInfo<T, K> {

  private final ConvertType convertType;
  private final WrappedField parentField; // Field from NMS packet
  private final WrappedField wrappedPacketField; // Field from wrapped packet
  private final List<Function<T, K>> computations;

  @SafeVarargs
  public ConvertInfo(ConvertType convertType, WrappedField parentField, WrappedField childField, Function<T, K>... computations) {
    this.convertType = convertType;
    this.parentField = parentField;
    this.wrappedPacketField = childField;
    this.computations = Lists.newArrayList(computations);
  }

  public void define(Object wrappedPacket, Object packet) {
    Object value = this.parentField.get(packet);
    switch (this.convertType) {
      case FLOAT:
        value = ((Number) value).floatValue();
        break;
      case DOUBLE:
        value = ((Number) value).doubleValue();
        break;
      case INTEGER:
        value = ((Number) value).intValue();
        break;
      case BYTE:
        value = ((Number) value).byteValue();
        break;
      case SHORT:
        value = ((Number) value).shortValue();
        break;
      default:
        break;
    }
    for (Function<T, K> function : computations) {
      value = function.apply((T) value);
    }
    this.wrappedPacketField.set(wrappedPacket, value);
  }
}
