package com.heirteir.susanobase.api.packets.wrappers.builder;

import com.heirteir.susanobase.api.SusanoAPI;
import com.heirteir.susanobase.api.util.reflections.Version;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public final class DefinitionChecker {

  private final Version version;
  private final Modifier modifier;

  public boolean valid(SusanoAPI api) {
    boolean valid;
    if (this.modifier.equals(Modifier.GREATER_THAN_OR_EQUAL)) {
      valid = api.getReflections().getVersion().greaterThanOrEqual(this.version);
    } else {
      valid = api.getReflections().getVersion().lessThanOrEqual(this.version);
    }
    return valid;
  }

  public enum Modifier {
    LESS_THAN_OR_EQUAL,
    GREATER_THAN_OR_EQUAL
  }
}
