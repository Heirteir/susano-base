package com.heirteir.susanobase.api.packets.handling.channel;

import com.heirteir.susanobase.api.packets.handling.PacketManager;
import com.heirteir.susanobase.api.player.SusanoPlayer;

final class ChannelHandlerCompat8 extends io.netty.channel.ChannelDuplexHandler implements ChannelHandler {

  private final PacketManager packetManager;
  private final SusanoPlayer player;

  protected ChannelHandlerCompat8(PacketManager packetManager, SusanoPlayer player) {
    super();
    this.packetManager = packetManager;
    this.player = player;
  }

  @Override
  public void write(io.netty.channel.ChannelHandlerContext ctx, Object msg, io.netty.channel.ChannelPromise promise) throws Exception {
    super.write(ctx, msg, promise);
    this.packetManager.handle(this.player, msg);
  }

  @Override
  public void channelRead(io.netty.channel.ChannelHandlerContext ctx, Object msg) throws Exception {
    super.channelRead(ctx, msg);
    this.packetManager.handle(this.player, msg);
  }
}
