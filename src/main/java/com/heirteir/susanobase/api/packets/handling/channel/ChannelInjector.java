package com.heirteir.susanobase.api.packets.handling.channel;

import com.google.common.collect.Sets;
import com.heirteir.susanobase.api.SusanoAPI;
import com.heirteir.susanobase.api.packets.handling.PacketManager;
import com.heirteir.susanobase.api.player.SusanoPlayer;
import com.heirteir.susanobase.api.util.reflections.Version;
import com.heirteir.susanobase.api.util.reflections.types.WrappedField;
import com.heirteir.susanobase.api.util.reflections.types.WrappedMethod;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.bukkit.entity.Player;

public final class ChannelInjector {

  private final SusanoAPI api;
  private final PacketManager packetManager;
  private final Set<ChannelHandler> activeHandlers;
  private final ExecutorService channelChangeExecutor;
  // compat
  private final boolean compat7; // true if minecraft version is <=1.7
  // reflections
  private final WrappedField channel;
  private final WrappedField networkManager;
  private final WrappedMethod pipeline;
  private final WrappedMethod get;
  private final WrappedMethod remove;
  private final WrappedMethod addBefore;

  public ChannelInjector(SusanoAPI api, PacketManager packetManager) {
    this.api = api;
    this.packetManager = packetManager;
    this.activeHandlers = Sets.newHashSet();
    this.channelChangeExecutor = Executors.newSingleThreadExecutor();
    this.compat7 = this.api.getReflections().getVersion().equals(Version.SEVEN_R4);
    String packageName = this.compat7 ? "net.minecraft.util.io.netty.channel." : "io.netty.channel.";
    this.channel = this.api.getReflections().getNetMinecraftServerClass("NetworkManager").getFieldByType(this.api.getReflections().getClass(packageName + "Channel").getRaw(), 0);

    this.pipeline = this.api.getReflections().getClass(packageName + "Channel").getMethod("pipeline");

    this.get = this.api.getReflections().getClass(packageName + "ChannelPipeline").getMethod("get", String.class);

    this.remove = this.api.getReflections().getClass(packageName + "ChannelPipeline").getMethod("remove", String.class);

    this.addBefore = this.api.getReflections()
        .getClass(packageName + "ChannelPipeline")
        .getMethod("addBefore", String.class, String.class, this.api.getReflections().getClass(packageName + "ChannelHandler").getRaw());

    this.networkManager = this.api.getReflections().getNetMinecraftServerClass("PlayerConnection").getFieldByName("networkManager");
  }

  public void unload() {
    this.channelChangeExecutor.shutdown();
    this.activeHandlers.clear();
  }

  public void addChannel(SusanoPlayer player) {
    this.channelChangeExecutor.execute(() -> {
      Object pipeline = this.getPipeline(player.getBukkitPlayer());
      this.quickRemove(pipeline);
      ChannelHandler handler = this.compat7 ? new ChannelHandlerCompat7(this.packetManager, player) : new ChannelHandlerCompat8(this.packetManager, player);
      this.activeHandlers.add(handler);
      this.addBefore.invoke(pipeline, Keys.HANDLER_KEY, Keys.PLAYER_KEY, handler);
    });
  }

  public void removeChannel(SusanoPlayer player) {
    this.channelChangeExecutor.execute(() -> this.quickRemove(this.getPipeline(player.getBukkitPlayer())));
  }

  private void quickRemove(Object pipeline) {
    Object handler = this.get.invoke(pipeline, Keys.PLAYER_KEY);
    if (handler != null) {
      if (handler instanceof ChannelHandler) {
        this.activeHandlers.remove(handler);
      }
      this.remove.invoke(pipeline, Keys.PLAYER_KEY);
    }
  }

  private Object getPipeline(Player player) {
    return this.pipeline.invoke(this.channel.get(this.networkManager.get(this.api.getReflections().getHelper().getPlayerHelper().getPlayerConnection(player))));
  }

  private static final class Keys {

    private static final String PLAYER_KEY = "susano_player_handler";
    private static final String HANDLER_KEY = "packet_handler";
  }
}
