package com.heirteir.susanobase.api.packets.wrappers;

import com.heirteir.susanobase.api.packets.PacketType;

public interface WrappedPacket {

  PacketType getPacketType();

  Class<?> getBaseClass();
}
