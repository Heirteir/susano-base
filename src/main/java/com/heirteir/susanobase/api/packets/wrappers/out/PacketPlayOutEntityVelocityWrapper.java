package com.heirteir.susanobase.api.packets.wrappers.out;

import com.heirteir.susanobase.api.packets.PacketType;
import lombok.Getter;

@Getter
public class PacketPlayOutEntityVelocityWrapper extends PacketPlayOutWrapper {

  public static final double CONVERSION = 8000D;
  private double x;
  private double y;
  private double z;

  public PacketPlayOutEntityVelocityWrapper(Class<?> baseClass) {
    super(PacketType.PacketPlayOutEntityVelocity, baseClass);
  }
}
