package com.heirteir.susanobase.api.packets.wrappers.builder;

import com.google.common.collect.Sets;
import com.heirteir.susanobase.api.SusanoAPI;
import com.heirteir.susanobase.api.packets.wrappers.WrappedPacket;
import com.heirteir.susanobase.api.packets.wrappers.builder.conversion.ConvertInfo;
import com.heirteir.susanobase.api.packets.wrappers.builder.conversion.ConvertType;
import com.heirteir.susanobase.api.util.reflections.Version;
import com.heirteir.susanobase.api.util.reflections.types.WrappedClass;
import com.heirteir.susanobase.api.util.reflections.types.WrappedConstructor;
import java.util.Set;
import java.util.function.Function;

public final class WrappedPacketBuilder {

  private final SusanoAPI api;
  private final WrappedClass packetClass;
  private final WrappedClass wrappedPacketClass;
  private final WrappedConstructor wrappedPacketConstructor;
  private final Set<ConvertInfo> definitions;
  private DefinitionChecker checker;

  protected WrappedPacketBuilder(SusanoAPI api, String parentPacketName, Class<? extends WrappedPacket> wrappedPacketClass) {
    this.api = api;
    this.packetClass = api.getReflections().getNetMinecraftServerClass(parentPacketName);
    this.wrappedPacketClass = api.getReflections().getClass(wrappedPacketClass.getName());
    this.wrappedPacketConstructor = this.wrappedPacketClass.getConstructor(Class.class);
    this.definitions = Sets.newHashSet();
    this.checker = new DefinitionChecker(Version.MIN, DefinitionChecker.Modifier.GREATER_THAN_OR_EQUAL);
  }

  public <T extends WrappedPacket> T build(Object packet) {
    T wrappedPacket = wrappedPacketConstructor.newInstance(packet.getClass());
    this.definitions.forEach(definition -> definition.define(wrappedPacket, packet));
    return wrappedPacket;
  }

  public WrappedPacketBuilder defineVersion(Version version, DefinitionChecker.Modifier modifier) {
    this.checker = new DefinitionChecker(version, modifier);
    return this;
  }

  @SafeVarargs
  public final <T, K> WrappedPacketBuilder addField(String parentName, String wrappedPacketName, ConvertType type, Function<T, K>... computations) {
    assert wrappedPacketClass.hasFieldByName(wrappedPacketName);
    if (this.checker.valid(this.api)) {
      this.definitions.add(new ConvertInfo<>(type, this.packetClass.getFieldByName(parentName), this.wrappedPacketClass.getFieldByName(wrappedPacketName), computations));
    }
    return this;
  }

  @SafeVarargs
  public final <T, K> WrappedPacketBuilder addField(String bothName, ConvertType type, Function<T, K>... computations) {
    return this.addField(bothName, bothName, type, computations);
  }

  @SafeVarargs
  public final <T, K> WrappedPacketBuilder addField(String parentName, String wrappedPacketName, Function<T, K>... computations) {
    return this.addField(parentName, wrappedPacketName, ConvertType.NONE, computations);
  }

  @SafeVarargs
  public final WrappedPacketBuilder addField(String bothName, Function<Object, Object>... computations) {
    return this.addField(bothName, bothName, ConvertType.NONE, computations);
  }
}
