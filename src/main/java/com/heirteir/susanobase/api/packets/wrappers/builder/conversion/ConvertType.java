package com.heirteir.susanobase.api.packets.wrappers.builder.conversion;

public enum ConvertType {
  BYTE,
  DOUBLE,
  FLOAT,
  INTEGER,
  SHORT,
  NONE
}
