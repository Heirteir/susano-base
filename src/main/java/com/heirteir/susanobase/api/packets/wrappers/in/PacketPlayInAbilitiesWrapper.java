package com.heirteir.susanobase.api.packets.wrappers.in;

import com.heirteir.susanobase.api.packets.PacketType;
import com.heirteir.susanobase.api.packets.wrappers.WrappedPacket;
import lombok.Getter;

@Getter
public class PacketPlayInAbilitiesWrapper implements WrappedPacket {

  private final PacketType packetType;
  private final Class<?> baseClass;
  private boolean flying;

  public PacketPlayInAbilitiesWrapper(Class<?> baseClass) {
    this.packetType = PacketType.PacketPlayInAbilities;
    this.baseClass = baseClass;
  }
}
