package com.heirteir.susanobase.api.packets.wrappers.in;

import com.heirteir.susanobase.api.packets.PacketType;
import com.heirteir.susanobase.api.packets.wrappers.WrappedPacket;
import java.util.Arrays;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
public class PacketPlayInEntityActionWrapper implements WrappedPacket {

  private final PacketType packetType;
  private final Class<?> baseClass;
  private PlayerAction action;

  public PacketPlayInEntityActionWrapper(Class<?> baseClass) {
    this.packetType = PacketType.PacketPlayInEntityAction;
    this.baseClass = baseClass;
  }

  @RequiredArgsConstructor
  @Getter
  public enum PlayerAction {
    START_SNEAKING(1),
    STOP_SNEAKING(2),
    START_SPRINTING(4),
    STOP_SPRINTING(5),
    START_FALL_FLYING(0),
    INVALID(0);
    private final int compat7Num;

    public static PlayerAction fromString(String name) {
      return Arrays.stream(PlayerAction.values()).filter(action -> action.name().equals(name)).findFirst().orElse(INVALID);
    }
  }
}
