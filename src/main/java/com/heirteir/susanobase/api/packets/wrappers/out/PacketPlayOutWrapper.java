package com.heirteir.susanobase.api.packets.wrappers.out;

import com.heirteir.susanobase.api.packets.PacketType;
import com.heirteir.susanobase.api.packets.wrappers.WrappedPacket;
import lombok.Getter;

@Getter
public class PacketPlayOutWrapper implements WrappedPacket {

  private final PacketType packetType;
  private final Class<?> baseClass;
  private int entityId;

  public PacketPlayOutWrapper(PacketType packetType, Class<?> baseClass) {
    this.packetType = packetType;
    this.baseClass = baseClass;
  }
}
