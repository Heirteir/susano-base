package com.heirteir.susanobase.api.packets.wrappers.builder;

import com.google.common.collect.Maps;
import com.heirteir.susanobase.api.SusanoAPI;
import com.heirteir.susanobase.api.packets.PacketType;
import com.heirteir.susanobase.api.packets.wrappers.builder.conversion.ConvertType;
import com.heirteir.susanobase.api.packets.wrappers.in.PacketPlayInEntityActionWrapper;
import com.heirteir.susanobase.api.packets.wrappers.out.PacketPlayOutEntityVelocityWrapper;
import com.heirteir.susanobase.api.util.reflections.Version;
import java.util.Arrays;
import java.util.Map;

public final class WrappedPacketFactory {

  private final SusanoAPI api;
  private final Map<PacketType, WrappedPacketBuilder> wrappedPacketBuilders;

  public WrappedPacketFactory(SusanoAPI api) {
    this.api = api;
    this.wrappedPacketBuilders = Maps.newHashMap();
  }

  public void load() {
    this.createWrappedPacketBuilder(PacketType.PacketPlayInFlying)
        .addField("x", ConvertType.DOUBLE)
        .addField("y", ConvertType.DOUBLE)
        .addField("z", ConvertType.DOUBLE)
        .addField("yaw", ConvertType.DOUBLE)
        .addField("pitch", ConvertType.DOUBLE)
        .addField("hasPos")
        .addField("hasLook")
        .defineVersion(Version.SEVEN_R4, DefinitionChecker.Modifier.LESS_THAN_OR_EQUAL)
        .addField("g", "onGround")
        .defineVersion(Version.EIGHT_R1, DefinitionChecker.Modifier.GREATER_THAN_OR_EQUAL)
        .addField("f", "onGround");

    this.createWrappedPacketBuilder(PacketType.PacketPlayInAbilities).addField("b", "flying");

    this.createWrappedPacketBuilder(PacketType.PacketPlayInEntityAction)
        .defineVersion(Version.SEVEN_R4, DefinitionChecker.Modifier.LESS_THAN_OR_EQUAL)
        .addField("animation",
            "action",
            (animation) -> Arrays.stream(PacketPlayInEntityActionWrapper.PlayerAction.values())
                .filter(action -> ((int) animation) == action.getCompat7Num())
                .findFirst()
                .orElse(PacketPlayInEntityActionWrapper.PlayerAction.INVALID))
        .defineVersion(Version.EIGHT_R1, DefinitionChecker.Modifier.GREATER_THAN_OR_EQUAL)
        .addField("animation", "action", (animation) -> PacketPlayInEntityActionWrapper.PlayerAction.fromString(((Enum) animation).name()));

    this.createWrappedPacketBuilder(PacketType.PacketPlayOutEntityVelocity)
        .addField("a", "entityId")
        .addField("b", "x", ConvertType.DOUBLE, (x) -> (double) x / PacketPlayOutEntityVelocityWrapper.CONVERSION)
        .addField("c", "y", ConvertType.DOUBLE, (y) -> (double) y / PacketPlayOutEntityVelocityWrapper.CONVERSION)
        .addField("d", "z", ConvertType.DOUBLE, (z) -> (double) z / PacketPlayOutEntityVelocityWrapper.CONVERSION);
  }

  public WrappedPacketBuilder createWrappedPacketBuilder(PacketType type) {
    assert !this.wrappedPacketBuilders.containsKey(type);
    WrappedPacketBuilder builder = new WrappedPacketBuilder(this.api, type.name(), type.getWrappedPacketClass());
    this.wrappedPacketBuilders.put(type, builder);
    return builder;
  }

  public WrappedPacketBuilder getWrappedPacketBuilder(PacketType type) {
    WrappedPacketBuilder builder = this.wrappedPacketBuilders.get(type);
    assert builder != null;
    return builder;
  }
}
