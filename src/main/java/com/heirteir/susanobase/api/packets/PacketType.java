package com.heirteir.susanobase.api.packets;

import com.google.common.collect.Sets;
import com.heirteir.susanobase.api.packets.wrappers.WrappedPacket;
import com.heirteir.susanobase.api.packets.wrappers.in.PacketPlayInAbilitiesWrapper;
import com.heirteir.susanobase.api.packets.wrappers.in.PacketPlayInEntityActionWrapper;
import com.heirteir.susanobase.api.packets.wrappers.in.PacketPlayInFlyingWrapper;
import com.heirteir.susanobase.api.packets.wrappers.out.PacketPlayOutEntityVelocityWrapper;
import java.util.Arrays;
import java.util.Set;
import lombok.Getter;

public enum PacketType {
  PacketPlayInFlying(PacketDirection.IN, PacketPlayInFlyingWrapper.class, "PacketPlayInPositionLook", "PacketPlayInPosition", "PacketPlayInLook"),
  PacketPlayInAbilities(PacketDirection.IN, PacketPlayInAbilitiesWrapper.class),
  PacketPlayInEntityAction(PacketDirection.IN, PacketPlayInEntityActionWrapper.class),
  PacketPlayOutEntityVelocity(PacketDirection.OUT, PacketPlayOutEntityVelocityWrapper.class),
  INVALID(PacketDirection.INVALID, null);
  @Getter
  private final PacketDirection direction;
  @Getter
  private final Class<? extends WrappedPacket> wrappedPacketClass;
  private final Set<String> names;

  PacketType(PacketDirection direction, Class<? extends WrappedPacket> wrappedPacketClass, String... names) {
    this.direction = direction;
    this.wrappedPacketClass = wrappedPacketClass;
    this.names = Sets.newHashSet(names);
  }

  public static PacketType fromString(String string) {
    return Arrays.stream(PacketType.values()).filter(packetType -> packetType.name().equals(string) || packetType.names.contains(string)).findFirst().orElse(PacketType.INVALID);
  }

  public enum PacketDirection {
    IN,
    OUT,
    INVALID
  }
}
