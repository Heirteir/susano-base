package com.heirteir.susanobase.api.packets.handling;

import com.heirteir.susanobase.api.SusanoAPI;
import com.heirteir.susanobase.api.packets.PacketType;
import com.heirteir.susanobase.api.packets.handling.channel.ChannelInjector;
import com.heirteir.susanobase.api.packets.wrappers.WrappedPacket;
import com.heirteir.susanobase.api.packets.wrappers.builder.WrappedPacketFactory;
import com.heirteir.susanobase.api.packets.wrappers.out.PacketPlayOutWrapper;
import com.heirteir.susanobase.api.player.SusanoPlayer;
import lombok.Getter;

public final class PacketManager {

  private final SusanoAPI api;
  @Getter
  private final ChannelInjector channelInjector;
  private final WrappedPacketFactory wrappedPacketFactory;

  public PacketManager(SusanoAPI api) {
    this.api = api;
    this.channelInjector = new ChannelInjector(api, this);
    this.wrappedPacketFactory = new WrappedPacketFactory(api);
  }

  public void load() {
    this.wrappedPacketFactory.load();
  }

  public void unload() {
    this.channelInjector.unload();
  }

  public void handle(SusanoPlayer player, Object packet) {
    player.runTask(() -> {
      PacketType type = PacketType.fromString(packet.getClass().getSimpleName());
      if (type.equals(PacketType.INVALID)) {
        return;
      }
      WrappedPacket wrappedPacket = this.wrappedPacketFactory.getWrappedPacketBuilder(type).build(packet);
      if (type.getDirection().equals(PacketType.PacketDirection.IN)
          || type.getDirection().equals(PacketType.PacketDirection.OUT) && ((PacketPlayOutWrapper) wrappedPacket).getEntityId() == player.getBukkitPlayer().getEntityId()) {
        this.api.getEventManager().runPacketUpdaters(this.api, player, type, wrappedPacket);
      }
    });
  }
}
