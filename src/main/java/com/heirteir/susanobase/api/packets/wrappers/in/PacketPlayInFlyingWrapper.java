package com.heirteir.susanobase.api.packets.wrappers.in;

import com.heirteir.susanobase.api.packets.PacketType;
import com.heirteir.susanobase.api.packets.wrappers.WrappedPacket;
import lombok.Getter;

@Getter
public class PacketPlayInFlyingWrapper implements WrappedPacket {

  private final PacketType packetType;
  private final Class<?> baseClass;
  private double x;
  private double y;
  private double z;
  private double yaw;
  private double pitch;
  private boolean hasLook;
  private boolean hasPos;
  private boolean onGround;

  public PacketPlayInFlyingWrapper(Class<?> baseClass) {
    this.packetType = PacketType.PacketPlayInFlying;
    this.baseClass = baseClass;
  }
}
