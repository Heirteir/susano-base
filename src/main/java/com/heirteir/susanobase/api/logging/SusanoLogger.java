package com.heirteir.susanobase.api.logging;

import com.heirteir.susanobase.api.SusanoBaseGlobalVariables;
import java.io.File;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public final class SusanoLogger {

  private static final Logger logger = Logger.getLogger("Susano-Logger");
  private final SusanoBaseGlobalVariables variables;
  private final FileHandler loggingFileHandler;

  public SusanoLogger(SusanoBaseGlobalVariables variables) {
    this.variables = variables;
    FileHandler fileHandler = null;
    try {
      fileHandler = new FileHandler(new File(this.variables.getLoggingFolder(), "latest-log.txt").getAbsolutePath());
    } catch (IOException e) {
      this.severe(e.getMessage(), e);
    }
    this.loggingFileHandler = fileHandler;
    if (this.loggingFileHandler != null) {
      this.loggingFileHandler.setFormatter(new SimpleFormatter());
      SusanoLogger.logger.addHandler(this.loggingFileHandler);
    }
  }

  private String addPrefix(String message) {
    return this.generatePrefix() + message;
  }

  public void info(String message) {
    SusanoLogger.logger.info(this.addPrefix(message));
  }

  public void severe(String message, Exception exception) {
    SusanoLogger.logger.log(Level.SEVERE, this.addPrefix(message), exception);
  }

  public void unload() {
    this.loggingFileHandler.close();
  }
  // statics

  private String generatePrefix() {
    return "[" + this.variables + "] ";
  }
}
