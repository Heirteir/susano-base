package com.heirteir.susanobase.api.player.data.location.updaters;

import com.heirteir.susanobase.api.SusanoAPI;
import com.heirteir.susanobase.api.event.AbstractPacketEvent;
import com.heirteir.susanobase.api.event.data.EventInfo;
import com.heirteir.susanobase.api.event.data.EventLevel;
import com.heirteir.susanobase.api.event.data.EventType;
import com.heirteir.susanobase.api.packets.PacketType;
import com.heirteir.susanobase.api.packets.wrappers.in.PacketPlayInFlyingWrapper;
import com.heirteir.susanobase.api.player.SusanoPlayer;
import com.heirteir.susanobase.api.player.data.location.PlayerData;
import org.jetbrains.annotations.NotNull;

public final class PlayerDataLocationUpdater extends AbstractPacketEvent<PacketPlayInFlyingWrapper> {

  public PlayerDataLocationUpdater() {
    super(new EventInfo(EventLevel.PROCESS_1, EventType.PACKET), PacketType.PacketPlayInFlying);
  }

  @Override
  public boolean update(SusanoAPI api, SusanoPlayer player, @NotNull PacketPlayInFlyingWrapper flying) {
    PlayerData data = player.getDataManager().getData(PlayerData.class);
    data.update(flying);
    return true;
  }
}
