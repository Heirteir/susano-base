package com.heirteir.susanobase.api.player.data.location;

import com.heirteir.susanobase.api.SusanoAPI;
import com.heirteir.susanobase.api.player.SusanoPlayer;
import com.heirteir.susanobase.api.player.data.AbstractDataBuilder;
import com.heirteir.susanobase.api.player.data.location.updaters.PlayerDataAbilitiesUpdater;
import com.heirteir.susanobase.api.player.data.location.updaters.PlayerDataEntityActionUpdater;
import com.heirteir.susanobase.api.player.data.location.updaters.PlayerDataLocationUpdater;
import java.util.Set;

public final class PlayerDataBuilder extends AbstractDataBuilder<PlayerData> {

  private final PlayerDataAbilitiesUpdater abilitiesUpdater;
  private final PlayerDataEntityActionUpdater entityActionUpdater;
  private final PlayerDataLocationUpdater locationUpdater;

  public PlayerDataBuilder(SusanoAPI api) {
    super(api);
    this.abilitiesUpdater = new PlayerDataAbilitiesUpdater();
    this.entityActionUpdater = new PlayerDataEntityActionUpdater();
    this.locationUpdater = new PlayerDataLocationUpdater();
  }

  @Override
  public PlayerData build(SusanoPlayer player) {
    return new PlayerData(super.getApi(), player);
  }

  @Override
  public void registerUpdaters() {
    super.getApi().getEventManager().addPacketEvent(this.abilitiesUpdater);
    super.getApi().getEventManager().addPacketEvent(this.entityActionUpdater);
    super.getApi().getEventManager().addPacketEvent(this.locationUpdater);
  }

  @Override
  public void removeUpdaters() {
    super.getApi().getEventManager().removePacketEvent(this.abilitiesUpdater);
    super.getApi().getEventManager().removePacketEvent(this.entityActionUpdater);
    super.getApi().getEventManager().removePacketEvent(this.locationUpdater);
  }

  @Override
  public void unload(Set<SusanoPlayer> players) {
    
  }
}
