package com.heirteir.susanobase.api.player.data.location;

import com.heirteir.susanobase.api.SusanoAPI;
import com.heirteir.susanobase.api.packets.wrappers.in.PacketPlayInFlyingWrapper;
import com.heirteir.susanobase.api.player.SusanoPlayer;
import com.heirteir.susanobase.api.util.math.Vector2D;
import com.heirteir.susanobase.api.util.math.Vector3D;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
public final class PlayerData {

  private final SusanoAPI api;
  private final SusanoPlayer player;
  private Data current;
  private Data previous;

  public PlayerData(SusanoAPI api, SusanoPlayer player) {
    this.api = api;
    this.player = player;
    this.previous = new Data(new Vector2D(player.getBukkitPlayer().getEyeLocation()),
        new Vector3D(player.getBukkitPlayer().getLocation()),
        new Vector3D(0, 0, 0),
        true,
        false,
        false,
        false,
        false,
        false,
        false,
        false);
    this.current = this.previous;
  }

  public void update(PacketPlayInFlyingWrapper flying) {
    this.previous = this.current;
    this.current = new Data(this.previous);
    this.current.onGround = flying.isOnGround();
    this.current.hasLook = flying.isHasLook();
    this.current.hasPos = flying.isHasPos();
    this.current.velocity = new Vector3D(0, 0, 0);
    this.current.moving = false;
    if (this.current.hasLook) {
      this.current.direction = new Vector2D(flying.getYaw(), flying.getPitch());
    }
    if (this.current.hasPos) {
      this.current.location = new Vector3D(flying.getX(), flying.getY(), flying.getZ());
      this.current.velocity = this.current.location.copy().subtract(this.previous.location);
      this.current.moving = this.current.velocity.getX() != 0 || this.current.velocity.getY() != 0 || this.current.velocity.getZ() != 0;
    }
    this.setElytraFlying(this.api.getReflections().getHelper().getPlayerHelper().isElytraFlying(this.player.getBukkitPlayer()));
  }

  public void setSneaking(boolean sneaking) {
    this.current.sneaking = sneaking;
  }

  public void setSprinting(boolean sprinting) {
    this.current.sprinting = sprinting;
  }

  public void setElytraFlying(boolean elytraFlying) {
    this.current.elytraFlying = elytraFlying;
  }

  public void setFlying(boolean flying) {
    this.current.flying = flying;
  }

  @AllArgsConstructor
  @lombok.Data
  public static final class Data {

    private Vector2D direction;
    private Vector3D location;
    private Vector3D velocity;
    private boolean onGround;
    private boolean moving;
    private boolean sneaking;
    private boolean sprinting;
    private boolean elytraFlying;
    private boolean flying;
    private boolean hasPos;
    private boolean hasLook;

    private Data(Data current) {
      this(current.direction,
          current.location,
          current.velocity,
          current.onGround,
          current.moving,
          current.sneaking,
          current.sprinting,
          current.elytraFlying,
          current.flying,
          current.hasPos,
          current.hasLook);
    }
  }
}
