package com.heirteir.susanobase.api.player.data.location.updaters;

import com.heirteir.susanobase.api.SusanoAPI;
import com.heirteir.susanobase.api.event.AbstractPacketEvent;
import com.heirteir.susanobase.api.event.data.EventInfo;
import com.heirteir.susanobase.api.event.data.EventLevel;
import com.heirteir.susanobase.api.event.data.EventType;
import com.heirteir.susanobase.api.packets.PacketType;
import com.heirteir.susanobase.api.packets.wrappers.in.PacketPlayInAbilitiesWrapper;
import com.heirteir.susanobase.api.player.SusanoPlayer;
import com.heirteir.susanobase.api.player.data.location.PlayerData;
import org.jetbrains.annotations.NotNull;

public final class PlayerDataAbilitiesUpdater extends AbstractPacketEvent<PacketPlayInAbilitiesWrapper> {

  public PlayerDataAbilitiesUpdater() {
    super(new EventInfo(EventLevel.PROCESS_1, EventType.PACKET), PacketType.PacketPlayInAbilities);
  }

  @Override
  public boolean update(SusanoAPI api, SusanoPlayer player, @NotNull PacketPlayInAbilitiesWrapper packet) {
    PlayerData data = player.getDataManager().getData(PlayerData.class);
    data.setFlying(packet.isFlying());
    return true;
  }
}
