package com.heirteir.susanobase.api.player;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.heirteir.susanobase.api.SusanoAPI;
import com.heirteir.susanobase.api.util.compat.BukkitCompatibilityUtils;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public final class SusanoPlayerList implements Listener {

  private final SusanoAPI api;
  @Getter
  private final SusanoPlayerBuilder builder;
  private final Map<UUID, SusanoPlayer> players;

  public SusanoPlayerList(SusanoAPI api) {
    this.api = api;
    this.builder = new SusanoPlayerBuilder(this.api, this);
    this.players = Maps.newHashMap();
  }

  public SusanoPlayer getPlayer(Player player) {
    return this.players.computeIfAbsent(player.getUniqueId(), uuid -> {
      SusanoPlayer susanoPlayer = this.builder.build(player);
      this.api.getPacketManager().getChannelInjector().addChannel(susanoPlayer);
      return susanoPlayer;
    });
  }

  public void removePlayer(Player player) {
    this.api.getPacketManager().getChannelInjector().removeChannel(this.getPlayer(player));
    this.players.remove(player.getUniqueId());
  }

  public Set<SusanoPlayer> getAll() {
    return Sets.newHashSet(this.players.values());
  }

  public void load() {
    this.getBuilder().loadDefaults();
    Bukkit.getPluginManager().registerEvents(this, this.api.getBase());
    for (Player player : BukkitCompatibilityUtils.getAllPlayers()) {
      this.getPlayer(player);
    }
  }

  public void unload() {
    this.getBuilder().unloadDefaults();
    HandlerList.unregisterAll(this);
    for (Player player : BukkitCompatibilityUtils.getAllPlayers()) {
      this.removePlayer(player);
    }
    this.players.clear();
  }

  @EventHandler
  private void onPlayerJoin(PlayerJoinEvent e) { // NOPMD
    this.getPlayer(e.getPlayer());
  }

  @EventHandler
  private void onPlayerKick(PlayerKickEvent e) { // NOPMD
    this.removePlayer(e.getPlayer());
  }

  @EventHandler
  private void onPlayerQuit(PlayerQuitEvent e) { // NOPMD
    this.removePlayer(e.getPlayer());
  }
}
