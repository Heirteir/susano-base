package com.heirteir.susanobase.api.player.data.location.updaters;

import com.heirteir.susanobase.api.SusanoAPI;
import com.heirteir.susanobase.api.event.AbstractPacketEvent;
import com.heirteir.susanobase.api.event.data.EventInfo;
import com.heirteir.susanobase.api.event.data.EventLevel;
import com.heirteir.susanobase.api.event.data.EventType;
import com.heirteir.susanobase.api.packets.PacketType;
import com.heirteir.susanobase.api.packets.wrappers.in.PacketPlayInEntityActionWrapper;
import com.heirteir.susanobase.api.player.SusanoPlayer;
import com.heirteir.susanobase.api.player.data.location.PlayerData;
import org.jetbrains.annotations.NotNull;

public final class PlayerDataEntityActionUpdater extends AbstractPacketEvent<PacketPlayInEntityActionWrapper> {

  public PlayerDataEntityActionUpdater() {
    super(new EventInfo(EventLevel.PROCESS_1, EventType.PACKET), PacketType.PacketPlayInEntityAction);
  }

  @Override
  public boolean update(SusanoAPI api, SusanoPlayer player, @NotNull PacketPlayInEntityActionWrapper entityAction) {
    PlayerData data = player.getDataManager().getData(PlayerData.class);
    switch (entityAction.getAction()) {
      case START_SNEAKING:
        data.setSneaking(true);
        break;
      case STOP_SNEAKING:
        data.setSneaking(false);
        break;
      case START_FALL_FLYING:
        data.setElytraFlying(true);
        break;
      case START_SPRINTING:
        data.setSprinting(true);
        break;
      case STOP_SPRINTING:
        data.setSprinting(false);
        break;
      default:
        break;
    }
    return true;
  }
}
