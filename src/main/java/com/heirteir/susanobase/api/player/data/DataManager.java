package com.heirteir.susanobase.api.player.data;

import com.google.common.collect.Maps;
import java.util.Map;

@SuppressWarnings("unchecked")
public final class DataManager {

  private final Map<Class<?>, Object> data;

  public DataManager() {
    this.data = Maps.newHashMap();
  }

  /**
   * Attach data to the player to be used by plugins.
   *
   * @param clazz The class of the Data object
   * @param data  The data object
   */
  public void addData(Class<?> clazz, Object data) {
    this.data.put(clazz, data);
  }

  /**
   * Remove data from the player so it can be cleared from memory.
   *
   * @param clazz The class of the data type
   */
  public void removeData(Class<?> clazz) {
    this.data.remove(clazz);
  }

  /**
   * Retrieves the data object from the hashmap.
   *
   * @param clazz The class of the data object
   * @param <T>   The type for the object to be converted to on retrieval
   * @return The data object
   */
  public <T> T getData(Class<?> clazz) {
    return (T) this.data.get(clazz);
  }
}
