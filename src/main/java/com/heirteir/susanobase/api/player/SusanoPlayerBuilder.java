package com.heirteir.susanobase.api.player;

import com.google.common.collect.Maps;
import com.heirteir.susanobase.api.SusanoAPI;
import com.heirteir.susanobase.api.player.data.AbstractDataBuilder;
import com.heirteir.susanobase.api.player.data.location.PlayerData;
import com.heirteir.susanobase.api.player.data.location.PlayerDataBuilder;
import java.util.Map;
import org.bukkit.entity.Player;

public final class SusanoPlayerBuilder {

  private final SusanoPlayerList susanoPlayerList;
  private final Map<Class<?>, AbstractDataBuilder> builders;
  /* Default Data Builders */
  private final PlayerDataBuilder playerDataBuilder;

  protected SusanoPlayerBuilder(SusanoAPI api, SusanoPlayerList susanoPlayerList) {
    this.susanoPlayerList = susanoPlayerList;
    this.builders = Maps.newLinkedHashMap();
    this.playerDataBuilder = new PlayerDataBuilder(api);
  }

  public void registerDataBuilder(Class<?> clazz, AbstractDataBuilder builder) {
    this.builders.put(clazz, builder);
    this.susanoPlayerList.getAll().forEach(player -> player.getDataManager().addData(clazz, builder.build(player)));
    builder.registerUpdaters();
  }

  public void unregisterDataBuilder(Class<?> clazz) {
    AbstractDataBuilder builder = this.builders.remove(clazz);
    if (builder != null) {
      builder.removeUpdaters();
      builder.unload(this.susanoPlayerList.getAll());
    }
    this.susanoPlayerList.getAll().forEach(player -> player.getDataManager().removeData(clazz));
  }

  public void loadDefaults() {
    this.registerDataBuilder(PlayerData.class, this.playerDataBuilder);
  }

  public void unloadDefaults() {
    this.unregisterDataBuilder(PlayerData.class);
  }

  public SusanoPlayer build(Player player) {
    SusanoPlayer susanoPlayer = new SusanoPlayer(player);
    this.builders.forEach((key, value) -> susanoPlayer.getDataManager().addData(key, value.build(susanoPlayer)));
    return susanoPlayer;
  }
}
