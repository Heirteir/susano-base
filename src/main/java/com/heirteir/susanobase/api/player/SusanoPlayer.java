package com.heirteir.susanobase.api.player;

import com.heirteir.susanobase.api.player.data.DataManager;
import java.util.concurrent.CompletableFuture;
import lombok.AccessLevel;
import lombok.Getter;
import org.bukkit.entity.Player;

@Getter
public class SusanoPlayer {

  @Getter(AccessLevel.NONE)
  private CompletableFuture<Void> future;
  private final Player bukkitPlayer;
  private final DataManager dataManager;

  protected SusanoPlayer(Player bukkitPlayer) {
    this.bukkitPlayer = bukkitPlayer;
    this.dataManager = new DataManager();
  }

  public void runTask(Runnable runnable) {
    if (this.future == null) {
      this.future = CompletableFuture.runAsync(runnable);
    } else {
      this.future = this.future.thenRunAsync(runnable);
    }
  }
}
