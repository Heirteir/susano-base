package com.heirteir.susanobase.api.player.data;

import com.heirteir.susanobase.api.SusanoAPI;
import com.heirteir.susanobase.api.player.SusanoPlayer;
import java.util.Set;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class AbstractDataBuilder<T> {

  @Getter(AccessLevel.PROTECTED)
  private final SusanoAPI api;

  public abstract T build(SusanoPlayer player);

  public abstract void registerUpdaters();

  public abstract void removeUpdaters();

  public abstract void unload(Set<SusanoPlayer> players);
}
