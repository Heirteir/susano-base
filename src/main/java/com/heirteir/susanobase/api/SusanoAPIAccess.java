package com.heirteir.susanobase.api;

import lombok.Getter;

public final class SusanoAPIAccess {

  @Getter
  private static SusanoAPI instance;

  private SusanoAPIAccess() {}

  public static void load(SusanoBase base) {
    SusanoAPIAccess.instance = new SusanoAPI(base);
  }

  public static void unload() {
    if (SusanoAPIAccess.instance != null) {
      SusanoAPIAccess.instance.unload();
    }
  }
}
